# SKYHAWK
小米12X SKYHAWK Recovery  
#### Linux
```
./Xiaomi_12X_boot_SHRP.sh
```

#### Windows
双击`Xiaomi_12X_boot_SHRP.bat`

#### 手动
```
fastboot flash vendor_boot vendor_boot.img
fastboot reboot recovery
```
注：  
1. **运行完Rec会被替换成第三方**  
2. **Rec保持关闭震动就会流畅**
